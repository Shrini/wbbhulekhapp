package com.effigrity.wbbhulekh.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Shrinivas on 20-Nov-21.
 */
public class CommonData {
    static final String baseURL = "http://effigrity.in:7070/saatbara/upbhulekh/";
    public static final String USER_AGENT = "Mozilla/4.0";
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded; charset=UTF-8";
    public static final String appInterstitial = "ca-app-pub-7199839993089502/8052863846";
    public static final String appName = "com.effigrity.uttarpradeshbhulekh";
    public static final String versionUrl = CommonData.baseURL + "version";

    public static String DISTRICT_LIST = "district_list";
    public static String TEHSIL_LIST = "tehsil_list";
    public static String VILLAGE_LIST = "village_list";

    public static String CHOSEN_DISTRICT = "chosen_district";
    public static String CHOSEN_TEHSIL = "chosen_tehsil";
    public static String CHOSEN_VILLAGE = "chosen_village";

    public static String CHOSEN_DISTRICT_CODE = "chosen_district_code";
    public static String CHOSEN_TEHSIL_CODE = "chosen_tehsil_code";
    public static String CHOSEN_VILLAGE_CODE = "chosen_village_code";

    public static String CHOSEN_DISTRICT_LOCALE = "chosen_district_locale";
    public static String CHOSEN_TEHSIL_LOCALE = "chosen_tehsil_locale";
    public static String CHOSEN_VILLAGE_LOCALE = "chosen_village_locale";

    public static String PARGANA_NAME = "prangana_name";
    public static String PARGANA_CODE = "prangana_code";

    public static String SEARCH_RESULT = "search_result";

    public static String FASLI_NAME = "fasli_name";
    public static String FASLI_CODE = "fasli_code";
    public static String ACTION = "action";
    public static String KHATA_NUMBER = "khata_number";
    public static final String DISPLAY_NAME = "display_name";

    public static String result = null;
    public static final String SAVED_CONTENT = "saved_content";

    static public final class UttarPradesh {
        public static final String baseURL = "http://upbhulekh.gov.in/";
        public static final String host = "upbhulekh.gov.in";
        public static final String origin = "http://upbhulekh.gov.in";
        public static final String referer = "http://upbhulekh.gov.in/public/public_ror/Public_ROR.jsp";
        public static final String bhulekhCopy = "http://upbhulekh.gov.in/public/public_ror/action/public_action.jsp";
        public static final String captchaURL = "http://upbhulekh.gov.in/public/public_ror/action/capImage.jsp";
        public static final String anchCopy = "http://upbhulekh.gov.in/public/public_ror/public_ror_report_ansh.jsp";
    }

    public static String savedContent = null;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }
}
