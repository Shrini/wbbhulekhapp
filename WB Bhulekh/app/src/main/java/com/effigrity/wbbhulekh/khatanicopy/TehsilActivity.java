package com.effigrity.wbbhulekh.khatanicopy;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.DashboardActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.model.Tehsil;
import com.effigrity.wbbhulekh.util.CommonData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class TehsilActivity extends DashboardActivity {

    ListView listView;
    ProgressBar progressBar;

    AdView adView;
    ArrayList<Tehsil> tehsils;
    TehsilAdapter tehsilAdapter;
    CoordinatorLayout coordinatorLayout;
    String districtName, districtCode, districtNameLocale;
    TextView district;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tehsil);

        listView = findViewById(R.id.list);
        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        district = findViewById(R.id.districtName);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.khaitani_copy_title));

        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        tehsils = (ArrayList<Tehsil>) getIntent().getSerializableExtra(CommonData.TEHSIL_LIST);
        districtName = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT);
        districtNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_LOCALE);
        districtCode = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_CODE);

        district.setText(districtNameLocale);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView.setAdapter(null);
        tehsilAdapter = new TehsilAdapter(this, tehsils, districtName, districtCode, districtNameLocale, coordinatorLayout, progressBar);
        listView.setAdapter(tehsilAdapter);
    }
}
