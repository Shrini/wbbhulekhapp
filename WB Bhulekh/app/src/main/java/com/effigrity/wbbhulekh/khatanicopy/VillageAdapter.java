package com.effigrity.wbbhulekh.khatanicopy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.model.Village;
import com.effigrity.wbbhulekh.util.CommonData;

import java.util.ArrayList;

public class VillageAdapter extends BaseAdapter implements
        View.OnClickListener {

    private Context activity;
    private ArrayList<Village> villages;
    private static LayoutInflater inflater = null;
    String districtName, districtCode, districtNameLocale;
    String tehsilName, tehsilCode, tehsilNameLocale;
    CoordinatorLayout coordinatorLayout;
    ProgressBar progressBar;

    public VillageAdapter(Context a, ArrayList<Village> villages, String districtName, String districtCode, String districtNameLocale, String tehsilName, String tehsilCode, String tehsilNameLocale, CoordinatorLayout coordinatorLayout, ProgressBar progressBar) {
        activity = a;
        this.villages = villages;
        this.districtName = districtName;
        this.districtCode = districtCode;
        this.districtNameLocale = districtNameLocale;

        this.tehsilName = tehsilName;
        this.tehsilCode = tehsilCode;
        this.tehsilNameLocale = tehsilNameLocale;

        this.coordinatorLayout = coordinatorLayout;
        this.progressBar = progressBar;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return villages.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.list_item, null);

        TextView name = vi.findViewById(R.id.name);
        name.setText((position + 1) + ") " + villages.get(position).getVname());

        vi.setTag(position);
        vi.setOnClickListener(this);
        return vi;
    }

    public void onClick(View v) {
        final int pos = (Integer) v.getTag();
        String villageName = villages.get(pos).getVname();
        String villageCode = villages.get(pos).getVillage_code_census();
        String parganaName = villages.get(pos).getPname();
        String parganaCode = villages.get(pos).getPargana_code_new();

        Intent intent = new Intent(activity, SearchInputActivity.class);

        intent.putExtra(CommonData.CHOSEN_DISTRICT, districtName);
        intent.putExtra(CommonData.CHOSEN_DISTRICT_CODE, districtCode);
        intent.putExtra(CommonData.CHOSEN_DISTRICT_LOCALE, districtNameLocale);

        intent.putExtra(CommonData.CHOSEN_TEHSIL, tehsilName);
        intent.putExtra(CommonData.CHOSEN_TEHSIL_CODE, tehsilCode);
        intent.putExtra(CommonData.CHOSEN_TEHSIL_LOCALE, tehsilNameLocale);

        intent.putExtra(CommonData.CHOSEN_VILLAGE, villageName);
        intent.putExtra(CommonData.CHOSEN_VILLAGE_CODE, villageCode);
        intent.putExtra(CommonData.CHOSEN_VILLAGE_LOCALE, villageName);

        intent.putExtra(CommonData.PARGANA_NAME, parganaName);
        intent.putExtra(CommonData.PARGANA_CODE, parganaCode);

        activity.startActivity(intent);
    }
}