package com.effigrity.wbbhulekh.asynctask;

import android.app.Activity;
import android.os.AsyncTask;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.effigrity.wbbhulekh.HomeActivity;
import com.effigrity.wbbhulekh.fragments.MainFragment;
import com.effigrity.wbbhulekh.model.AppVersion;
import com.effigrity.wbbhulekh.util.CommonData;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by I069067 on 20-Sep-16.
 */
public class CommonAsyncTask {

    private Activity activity;
    CoordinatorLayout coordinatorLayout;
    private Fragment fragment;

    public CommonAsyncTask(Activity a, Fragment passedFragment, CoordinatorLayout coordinatorLayout) {
        activity = a;
        fragment = passedFragment;
        this.coordinatorLayout = coordinatorLayout;
    }

    public void checkAppVersion() {
        if (CommonData.isNetworkAvailable(activity.getApplicationContext())) {
            new getVersion().execute();
        } else {

        }
    }

   /* public void checkIfMobileRequired() {
        if (CommonData.isNetworkAvailable(activity.getApplicationContext())) {
            new getIfMobileRequired().execute();
        } else {

        }
    }*/

    public class getVersion extends
            AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {

            String baseLink = CommonData.versionUrl;
            try {
                URL obj = new URL(baseLink);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                // add request header
                con.setRequestProperty("User-Agent", CommonData.USER_AGENT);
                con.setRequestProperty("Content-Type", CommonData.CONTENT_TYPE);

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'GET' request to URL : " + baseLink);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream(), "UTF-8"));
                String inputLine = "";
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                String result = response.toString();
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    AppVersion appVersion = mapper.readValue(result, AppVersion.class);
                    int currentVersion = CommonData.getAppVersion(activity);

                    if (currentVersion != 0 && (appVersion.getCurrent() != currentVersion && appVersion.getUpcoming() != currentVersion)) {
                        ((MainFragment) fragment).displaNotification();
                        ((HomeActivity) activity).showUpdateDialogue();
                    }
                } catch (Exception e) {

                }
            } else {

            }
        }
    }
}
