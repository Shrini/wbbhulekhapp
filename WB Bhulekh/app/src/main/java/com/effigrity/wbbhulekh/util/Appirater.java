package com.effigrity.wbbhulekh.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.effigrity.wbbhulekh.R;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;


/*
 * @source https://github.com/sbstrm/appirater-android
 * @license MIT/X11
 *
 * Copyright (c) 2011-2013 sbstrm Y.K.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

public class Appirater {

    private static final String PREF_LAUNCH_COUNT = "launch_count";
    private static final String PREF_EVENT_COUNT = "event_count";
    private static final String PREF_RATE_CLICKED = "rateclicked";
    private static final String PREF_DONT_SHOW = "dontshow";
    private static final String PREF_DATE_REMINDER_PRESSED = "date_reminder_pressed";
    private static final String PREF_DATE_FIRST_LAUNCHED = "date_firstlaunch";
    private static final String PREF_APP_VERSION_CODE = "versioncode";

    private static final String PREF_DATE_IN_APP_REVIEW_SHOWN = "date_in_app_review_shown";

    public static void appLaunched(Context mContext, Activity activity) {
        boolean testMode = mContext.getResources().getBoolean(
                R.bool.appirator_test_mode);
        SharedPreferences prefs = mContext.getSharedPreferences(
                mContext.getPackageName() + ".appirater", 0);
        if (!testMode
                && (prefs.getBoolean(PREF_DONT_SHOW, false) || prefs
                .getBoolean(PREF_RATE_CLICKED, false))) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        if (testMode) {
            Toast.makeText(mContext, "in Test mode. In app review", Toast.LENGTH_SHORT);
            return;
        }

        // Increment launch counter
        long launch_count = prefs.getLong(PREF_LAUNCH_COUNT, 0);

        // Get events counter
        long event_count = prefs.getLong(PREF_EVENT_COUNT, 0);

        // Get date of first launch
        long date_firstLaunch = prefs.getLong(PREF_DATE_FIRST_LAUNCHED, 0);

        long date_inAppReviewShown = prefs.getLong(PREF_DATE_IN_APP_REVIEW_SHOWN, 0);

        // Get reminder date pressed
        long date_reminder_pressed = prefs.getLong(PREF_DATE_REMINDER_PRESSED,
                0);

        try {
            int appVersionCode = mContext.getPackageManager().getPackageInfo(
                    mContext.getPackageName(), 0).versionCode;
            if (prefs.getInt(PREF_APP_VERSION_CODE, 0) != appVersionCode) {
                // Reset the launch and event counters to help assure users are
                // rating based on the latest version.
                launch_count = 0;
                event_count = 0;
                editor.putLong(PREF_EVENT_COUNT, event_count);
            }
            editor.putInt(PREF_APP_VERSION_CODE, appVersionCode);
        } catch (Exception e) {
            // do nothing
        }

        launch_count++;
        editor.putLong(PREF_LAUNCH_COUNT, launch_count);

        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong(PREF_DATE_FIRST_LAUNCHED, date_firstLaunch);
        }

        // Wait at least n days or m events before opening
        /*if (launch_count >= mContext.getResources().getInteger(
                R.integer.appirator_launches_until_prompt)) {
            long millisecondsToWait = mContext.getResources().getInteger(
                    R.integer.appirator_days_until_prompt)
                    * 24 * 60 * 60 * 1000L;
            if ((System.currentTimeMillis() >= (date_firstLaunch + millisecondsToWait)
                    || event_count >= mContext.getResources().getInteger(
                    R.integer.appirator_events_until_prompt)) && System.currentTimeMillis() > date_inAppReviewShown) {

                ReviewManager manager = ReviewManagerFactory.create(mContext);
                Task<ReviewInfo> request = manager.requestReviewFlow();
                request.addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        // We can get the ReviewInfo object
                        ReviewInfo reviewInfo = task.getResult();

                        // The In app review will not be shows before 24 hours.
                        editor.putLong(PREF_DATE_IN_APP_REVIEW_SHOWN, (millisecondsToWait + System.currentTimeMillis()));
                        editor.commit();

                        Task<Void> flow = manager.launchReviewFlow(activity, reviewInfo);
                        flow.addOnCompleteListener(reviewTask -> {


                        });

                    } else {
                        // There was some problem, continue regardless of the result.
                    }
                });
            }
        }*/
        editor.commit();
    }

    public static void significantEvent(Context mContext, Activity activity) {
        SharedPreferences prefs = mContext.getSharedPreferences(
                mContext.getPackageName() + ".appirater", 0);

        long event_count = prefs.getLong(PREF_EVENT_COUNT, 0);
        event_count++;
        prefs.edit().putLong(PREF_EVENT_COUNT, event_count).apply();
    }

    public static void showInAppReview(Context mContext, Activity activity) {
        SharedPreferences prefs = mContext.getSharedPreferences(
                mContext.getPackageName() + ".appirater", 0);

        long event_count = prefs.getLong(PREF_EVENT_COUNT, 0);
        // Increment launch counter
        long launch_count = prefs.getLong(PREF_LAUNCH_COUNT, 0);

        // Get date of first launch
        long date_firstLaunch = prefs.getLong(PREF_DATE_FIRST_LAUNCHED, 0);

        long date_inAppReviewShown = prefs.getLong(PREF_DATE_IN_APP_REVIEW_SHOWN, 0);

        // Wait at least n days or m events before opening
        if (launch_count >= mContext.getResources().getInteger(
                R.integer.appirator_launches_until_prompt)) {
            long millisecondsToWait = mContext.getResources().getInteger(
                    R.integer.appirator_days_until_prompt)
                    * 24 * 60 * 60 * 1000L;
            if ((System.currentTimeMillis() >= (date_firstLaunch + millisecondsToWait)
                    || event_count >= mContext.getResources().getInteger(
                    R.integer.appirator_events_until_prompt)) && System.currentTimeMillis() > date_inAppReviewShown) {

                ReviewManager manager = ReviewManagerFactory.create(mContext);
                Task<ReviewInfo> request = manager.requestReviewFlow();
                request.addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        // We can get the ReviewInfo object
                        ReviewInfo reviewInfo = task.getResult();

                        // The In app review will not be shows before 24 hours.
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putLong(PREF_DATE_IN_APP_REVIEW_SHOWN, (millisecondsToWait + System.currentTimeMillis()));
                        editor.commit();

                        Task<Void> flow = manager.launchReviewFlow(activity, reviewInfo);
                        flow.addOnCompleteListener(reviewTask -> {

                        });

                    } else {
                        // There was some problem, continue regardless of the result.
                    }
                });
            }
        }
    }
}