package com.effigrity.wbbhulekh;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.effigrity.wbbhulekh.util.CommonData;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;


public abstract class DashboardActivity extends AppCompatActivity {

    static final int VR_REQUEST = 999;
    private static final String INPUT = "INPUT";
    private static final String FASLI_NAME = "fasliName";
    private static final String ACTION = "action";

    public static InterstitialAd mInterstitialAd;
    static int noOfTimes = 0;
    public static boolean showAd = false;

    public static boolean contentShown = false;
    //HomeWatcher mHomeWatcher = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static final String ZONE = "ZONE";
    public static final String DISTRICT = "district";
    public static final String TEHSIL = "tehsil";
    public static final String VILLAGE = "village";
    public static final String ERRORS = "ERRORS";

    protected static boolean screenFreeze = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DashboardActivity.contentShown = false;

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        List<String> testDevices = new ArrayList<>();
        testDevices.add(AdRequest.DEVICE_ID_EMULATOR);
        testDevices.add("D5D886F1C029BAB7A352A3B0B5113C22");

        RequestConfiguration requestConfiguration
                = new RequestConfiguration.Builder()
                .setTestDeviceIds(testDevices)
                .build();
        MobileAds.setRequestConfiguration(requestConfiguration);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        /*try {
            if (noOfTimes <= 1) {
                mHomeWatcher = new HomeWatcher(this);
                mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
                    @Override
                    public void onHomePressed() {
                        showAd = true;
                    }

                    @Override
                    public void onHomeLongPressed() {
                        showAd = true;
                    }
                });
                mHomeWatcher.startWatch();
            }
        } catch (Exception e) {

        }*/

        try {
            if (mInterstitialAd == null) {
                mInterstitialAd = new InterstitialAd(getApplicationContext());
                mInterstitialAd.setAdUnitId(CommonData.appInterstitial);

                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        requestNewInterstitial();
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                        requestNewInterstitial();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        System.out.print(errorCode);
                        requestNewInterstitial();
                    }
                });

                requestNewInterstitial();
            }
        } catch (Exception e) {

        }
    }

    protected void logFirebaseEventAppOpen() {
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, null);
    }

    protected void logFirebaseEvent(String eventName) {
        mFirebaseAnalytics.logEvent(eventName, null);
    }


    public void logFirebaseErrorEvent(String district, String taluka, String village, String fasliName, String action) {
        Bundle bundle = new Bundle();
        bundle.putString(DISTRICT, district);
        bundle.putString(TEHSIL, taluka);
        bundle.putString(VILLAGE, village);
        bundle.putString(FASLI_NAME, fasliName);
        bundle.putString(ACTION, action);
        mFirebaseAnalytics.logEvent(ERRORS, bundle);
    }

    public void freezeScreen() {
        try {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            screenFreeze = true;
        } catch (Exception e) {

        }
    }

    public void unfreezeScreen() {
        try {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            screenFreeze = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean isScreenFreezed() {
        return screenFreeze;
    }

    protected void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelID = getString(R.string.channel_id);
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(channelID, name, importance);
            channel.setDescription(description);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void requestNewInterstitial() {
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    /**
     * onDestroy The final call you receive before your activity is destroyed.
     * This can happen either because the activity is finishing (someone called
     * finish() on it, or because the system is temporarily destroying this
     * instance of the activity to save space. You can distinguish between these
     * two scenarios with the isFinishing() method.
     */

    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * onPause Called when the system is about to start resuming a previous
     * activity. This is typically used to commit unsaved changes to persistent
     * data, stop animations and other things that may be consuming CPU, etc.
     * Implementations of this method must be very quick because the next
     * activity will not be resumed until this method returns. Followed by
     * either onResume() if the activity returns back to the front, or onStop()
     * if it becomes invisible to the user.
     */


    protected void onPause() {
        super.onPause();
        /*if (mInterstitialAd.isLoaded() && noOfTimes <= 1 && showAd) {
            mInterstitialAd.show();
            noOfTimes++;
        }
        showAd = false;*/
    }

    /**
     * onRestart Called after your activity has been stopped, prior to it being
     * started again. Always followed by onStart().
     */

    protected void onRestart() {
        super.onRestart();
    }

    /**
     * onResume Called when the activity will start interacting with the user.
     * At this point your activity is at the top of the activity stack, with
     * user input going to it. Always followed by onPause().
     */

    protected void onResume() {
        super.onResume();
    }

    /**
     * onStart Called when the activity is becoming visible to the user.
     * Followed by onResume() if the activity comes to the foreground, or
     * onStop() if it becomes hidden.
     */

    protected void onStart() {
        super.onStart();
    }

    /**
     * onStop Called when the activity is no longer visible to the user because
     * another activity has been resumed and is covering this one. This may
     * happen either because a new activity is being started, an existing one is
     * being brought in front of this one, or this one is being destroyed.
     * <p/>
     * Followed by either onRestart() if this activity is coming back to
     * interact with the user, or onDestroy() if this activity is going away.
     */

    protected void onStop() {
        super.onStop();
    }

    public void setError(final TextInputLayout inputLayoutMobile, final String msg, final View toFocusView) {

        inputLayoutMobile.setError(msg);
        requestFocus(toFocusView);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                inputLayoutMobile.setErrorEnabled(false);
            }
        }, 1500);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void onClickShareApp(View v) {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT,
                    "http://play.google.com/store/apps/details?id="
                            + CommonData.appName);
            startActivity(Intent.createChooser(intent, "Share"));
        } catch (Exception e) {

        }
    }


    public void takeVoiceInput(View v) {
        PackageManager packManager = getPackageManager();
        List<ResolveInfo> intActivities = packManager.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (intActivities.size() != 0) {
            listenToSpeech();
        } else {
            // speech recognition not supported, disable button and output
            // message
            toast_short("Oops - Speech recognition not supported!");
        }
    }

    private void listenToSpeech() {
        // start the speech recognition intent passing required data
        Intent listenIntent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        // set Language
        listenIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-UK");
        // start listening
        startActivityForResult(listenIntent, VR_REQUEST);
    }

    /**
     */
    // Click Methods

    /**
     * Handle the click on the home button.
     *
     * @param v View
     * @return void
     */

    public void onClickHome(View v) {

    }

    @Override
    public void onBackPressed() {
        if (!isScreenFreezed()) {
            super.onBackPressed();
            /*try {
                if (mHomeWatcher != null) {
                    mHomeWatcher.stopWatch();
                }
            } catch (Exception e) {

            }*/
        } else {
            toast_short(getResources().getString(R.string.working_on_request));
        }
    }

    public void BackActivity(View v) {
        onBackPressed();
    }

    /**
     * Handle the click on the About button.
     *
     * @param v View
     * @return void
     */

    public void onClickAbout(View v) {
        // startActivity(new Intent(getApplicationContext(), About.class));
    }

    // More Methods

    public void onClickGiveRating(View v) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + CommonData.appName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + CommonData.appName)));
        }
    }

    /**
     * Go back to the home activity.
     *
     * @param context Context
     * @return void
     */

    public void goHome(Context context) {

    }

    /**
     * Use the activity label to set the text in the activity's title text view.
     * The argument gives the name of the view.
     * <p/>
     * <p/>
     * This method is needed because we have a custom title bar rather than the
     * default Android title bar. See the theme definitons in styles.xml.
     *
     * @param textViewId int
     * @return void
     */

    public void setTitleFromActivityLabel(int textViewId) {
        TextView tv = (TextView) findViewById(textViewId);
        if (tv != null)
            tv.setText(getTitle());
    } // end setTitleText

    /**
     * Show a string on the screen via Toast.
     *
     * @param msg String
     * @return void
     */

    public void toast_long(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    } // end toast

    /**
     * Show a string on the screen via Toast.
     *
     * @param msg String
     * @return void
     */

    public void toast_short(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    } // end toast

    /**
     * Send a message to the debug log and display it using Toast.
     */
    public void trace(String msg) {
        Log.d("Demo", msg);
        toast_short(msg);
    }

    public void onFocusChange(View v, boolean hasFocus) {
        // TODO Auto-generated method stub

    }

} // end class
