package com.effigrity.wbbhulekh.asynctask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.effigrity.wbbhulekh.DashboardActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.khatanicopy.DistrictActivity;
import com.effigrity.wbbhulekh.khatanicopy.SearchResultActivity;
import com.effigrity.wbbhulekh.khatanicopy.TehsilActivity;
import com.effigrity.wbbhulekh.khatanicopy.VillageActivity;
import com.effigrity.wbbhulekh.model.District;
import com.effigrity.wbbhulekh.model.NameValue;
import com.effigrity.wbbhulekh.model.SearchResult;
import com.effigrity.wbbhulekh.model.Tehsil;
import com.effigrity.wbbhulekh.model.Village;
import com.effigrity.wbbhulekh.util.CommonData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PropertyAsyncTask {

    private Activity activity;
    private Fragment fragment;
    ProgressBar progressBar;

    CoordinatorLayout coordinatorLayout;
    java.net.CookieStore rawCookieStore;
    android.webkit.CookieManager webCookieManager;
    public static String cookieContent;

    Snackbar pd;

    public PropertyAsyncTask(Activity a, Fragment passedFragment, CoordinatorLayout coordinatorLayout, ProgressBar progressBar) {
        activity = a;
        fragment = passedFragment;
        this.coordinatorLayout = coordinatorLayout;
        this.progressBar = progressBar;
        this.progressBar.setVisibility(View.INVISIBLE);
        pd = Snackbar
                .make(coordinatorLayout, activity.getResources().getString(R.string.loading_data_please_wait), Snackbar.LENGTH_INDEFINITE);

        View snackBarView = pd.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));

        try {
            webCookieManager = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.createInstance(activity);
            }
            webCookieManager.setAcceptCookie(true);
            CookieHandler.setDefault(new java.net.CookieManager());
            rawCookieStore = ((java.net.CookieManager) CookieHandler.getDefault())
                    .getCookieStore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PropertyAsyncTask(Activity a, CoordinatorLayout coordinatorLayout, ProgressBar progressBar) {
        activity = a;
        this.coordinatorLayout = coordinatorLayout;
        this.progressBar = progressBar;
        this.progressBar.setVisibility(View.INVISIBLE);
        pd = Snackbar
                .make(coordinatorLayout, activity.getResources().getString(R.string.loading_data_please_wait), Snackbar.LENGTH_INDEFINITE);
        View snackBarView = pd.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        try {
            webCookieManager = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.createInstance(activity);
            }
            webCookieManager.setAcceptCookie(true);
            CookieHandler.setDefault(new java.net.CookieManager());
            rawCookieStore = ((java.net.CookieManager) CookieHandler.getDefault())
                    .getCookieStore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void freezeScreen() {
        ((DashboardActivity) activity).freezeScreen();
    }

    private void unfreezeScreen() {
        ((DashboardActivity) activity).unfreezeScreen();
    }

    public void getDistricts() {
        if (CommonData.isNetworkAvailable(activity.getApplicationContext())) {
            getDistrictsAync();
        } else {
            final Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, activity.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_INDEFINITE);

            snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getDistricts();
                    snackbar.dismiss();
                }
            });

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);

            snackbar.show();
        }
    }


    public void getDistrictsAync() {
        ExecutorService executors = Executors.newFixedThreadPool(1);

        Runnable runnable = new Runnable() {
            private void preExecuteOnUi() {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.show();
                        progressBar.setVisibility(View.VISIBLE);
                        freezeScreen();
                    }
                });
            }

            @Override
            public void run() {
                String result = null;
                HttpURLConnection con = null;
                try {
                    preExecuteOnUi();
                    String baseLink = CommonData.UttarPradesh.bhulekhCopy;

                    URL url = new URL(baseLink);
                    con = (HttpURLConnection)
                            url.openConnection();

                    con.setRequestMethod("POST");
                    con.setRequestProperty("User-Agent", CommonData.USER_AGENT);
                    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                    con.setRequestProperty("Content-Type", CommonData.CONTENT_TYPE);
                    con.setRequestProperty("Host", CommonData.UttarPradesh.host);
                    con.setRequestProperty("Origin", CommonData.UttarPradesh.origin);
                    con.setRequestProperty("Referer", CommonData.UttarPradesh.referer);

                    String urlParameters = "act=fillDistrict";

                    // Send post request
                    con.setDoOutput(true);
                    try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                        wr.writeBytes(urlParameters);
                        wr.flush();
                    }

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'POST' request to URL : " + url);
                    System.out.println("Post parameters : " + urlParameters);
                    System.out.println("Response Code : " + responseCode);

                    if (responseCode == 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream(), "UTF-8"));
                        String inputLine = "";
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();

                        java.net.URI baseUri = new URI(baseLink);
                        setCookies(baseUri);

                        result = response.toString();
                    } else {
                        result = Integer.toString(responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                }

                postExecuteOnUi(result);
            }


            private void postExecuteOnUi(final String result) {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            progressBar.setVisibility(View.INVISIBLE);
                            unfreezeScreen();
                            pd.dismiss();
                        } catch (Exception e) {
                            return;
                        }

                        if (result != null) {

                            if (result.contains("No Connection")) {
                                final Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG);

                                snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getDistricts();
                                        snackbar.dismiss();
                                    }
                                });

                                // Changing message text color
                                snackbar.setActionTextColor(Color.RED);

                                // Changing action button text color
                                View sbView = snackbar.getView();
                                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                                textView.setTextColor(Color.YELLOW);

                                snackbar.show();
                                logFirebaseErrorEvent("getDistricts", "", "", "", "");
                                return;
                            }

                            try {
                                ObjectMapper mapper = new ObjectMapper();
                                ArrayList<District> districts = mapper.readValue(result, new TypeReference<ArrayList<District>>() {
                                });

                                Intent intent = new Intent(activity, DistrictActivity.class);
                                intent.putExtra(CommonData.DISTRICT_LIST, districts);
                                activity.startActivity(intent);

                            } catch (Exception e) {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                logFirebaseErrorEvent("getDistricts", "", "", "", "");
                            }
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            logFirebaseErrorEvent("getDistricts", "", "", "", "");
                        }
                    }
                });
            }
        };
        executors.submit(runnable);
    }

    public void getTehsils(String districtName, String districtCode, String districtNameLocale) {
        if (CommonData.isNetworkAvailable(activity.getApplicationContext())) {
            getTehsilAync(districtName, districtCode, districtNameLocale);
        } else {
            final Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, activity.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_INDEFINITE);

            snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getTehsils(districtName, districtCode, districtNameLocale);
                    snackbar.dismiss();
                }
            });

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);

            snackbar.show();
        }
    }

    public void getTehsilAync(String districtName, String districtCode, String districtNameLocale) {
        ExecutorService executors = Executors.newFixedThreadPool(1);

        Runnable runnable = new Runnable() {
            private void preExecuteOnUi() {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.show();
                        progressBar.setVisibility(View.VISIBLE);
                        freezeScreen();
                    }
                });
            }

            @Override
            public void run() {
                String result = null;
                HttpURLConnection con = null;
                try {
                    preExecuteOnUi();
                    String baseLink = CommonData.UttarPradesh.bhulekhCopy;

                    URL url = new URL(baseLink);
                    con = (HttpURLConnection)
                            url.openConnection();

                    con.setRequestMethod("POST");
                    con.setRequestProperty("User-Agent", CommonData.USER_AGENT);
                    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                    con.setRequestProperty("Content-Type", CommonData.CONTENT_TYPE);
                    con.setRequestProperty("Host", CommonData.UttarPradesh.host);
                    con.setRequestProperty("Origin", CommonData.UttarPradesh.origin);
                    con.setRequestProperty("Referer", CommonData.UttarPradesh.referer);
                    con.setRequestProperty("Cookie", webCookieManager.getCookie(CommonData.UttarPradesh.baseURL));

                    String urlParameters = "act=fillTehsil&district_code=" + districtCode;

                    // Send post request
                    con.setDoOutput(true);
                    try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                        wr.writeBytes(urlParameters);
                        wr.flush();
                    }

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'POST' request to URL : " + url);
                    System.out.println("Post parameters : " + urlParameters);
                    System.out.println("Response Code : " + responseCode);

                    if (responseCode == 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream(), "UTF-8"));
                        String inputLine = "";
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        result = response.toString();
                    } else {
                        result = Integer.toString(responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                }
                postExecuteOnUi(result);
            }


            private void postExecuteOnUi(final String result) {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            progressBar.setVisibility(View.INVISIBLE);
                            unfreezeScreen();
                            pd.dismiss();
                        } catch (Exception e) {
                            return;
                        }

                        if (result != null) {
                            if (result.contains("No Connection")) {
                                final Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG);

                                snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getDistricts();
                                        snackbar.dismiss();
                                    }
                                });

                                // Changing message text color
                                snackbar.setActionTextColor(Color.RED);

                                // Changing action button text color
                                View sbView = snackbar.getView();
                                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                                textView.setTextColor(Color.YELLOW);

                                snackbar.show();
                                logFirebaseErrorEvent(districtName, "", "", "", "");
                                return;
                            }

                            try {
                                ObjectMapper mapper = new ObjectMapper();
                                ArrayList<Tehsil> tehsils = mapper.readValue(result, new TypeReference<ArrayList<Tehsil>>() {
                                });

                                Intent intent = new Intent(activity, TehsilActivity.class);
                                intent.putExtra(CommonData.TEHSIL_LIST, tehsils);
                                intent.putExtra(CommonData.CHOSEN_DISTRICT, districtName);
                                intent.putExtra(CommonData.CHOSEN_DISTRICT_CODE, districtCode);
                                intent.putExtra(CommonData.CHOSEN_DISTRICT_LOCALE, districtNameLocale);
                                activity.startActivity(intent);

                            } catch (Exception e) {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                logFirebaseErrorEvent(districtName, "", "", "", "");
                            }
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            logFirebaseErrorEvent(districtName, "", "", "", "");
                        }
                    }
                });
            }
        };
        executors.submit(runnable);
    }


    public void getVillages(String districtName, String districtCode, String districtNameLocale, String tehsilName, String tehsilCode, String tehsilNameLocale) {
        if (CommonData.isNetworkAvailable(activity.getApplicationContext())) {
            getVillageAync(districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale);
        } else {
            final Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, activity.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_INDEFINITE);

            snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getVillages(districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale);
                    snackbar.dismiss();
                }
            });

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);

            snackbar.show();
        }
    }

    public void getVillageAync(String districtName, String districtCode, String districtNameLocale, String tehsilName, String tehsilCode, String tehsilNameLocale) {
        ExecutorService executors = Executors.newFixedThreadPool(1);

        Runnable runnable = new Runnable() {
            private void preExecuteOnUi() {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.show();
                        progressBar.setVisibility(View.VISIBLE);
                        freezeScreen();
                    }
                });
            }

            @Override
            public void run() {
                String result = null;
                HttpURLConnection con = null;
                try {
                    preExecuteOnUi();
                    String baseLink = CommonData.UttarPradesh.bhulekhCopy;

                    URL url = new URL(baseLink);
                    con = (HttpURLConnection)
                            url.openConnection();

                    con.setRequestMethod("POST");
                    con.setRequestProperty("User-Agent", CommonData.USER_AGENT);
                    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                    con.setRequestProperty("Content-Type", CommonData.CONTENT_TYPE);
                    con.setRequestProperty("Host", CommonData.UttarPradesh.host);
                    con.setRequestProperty("Origin", CommonData.UttarPradesh.origin);
                    con.setRequestProperty("Referer", CommonData.UttarPradesh.referer);
                    con.setRequestProperty("Cookie", webCookieManager.getCookie(CommonData.UttarPradesh.baseURL));

                    String urlParameters = "act=fillVillage&district_code=" + districtCode + "&tehsil_code=" + tehsilCode;

                    // Send post request
                    con.setDoOutput(true);
                    try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                        wr.writeBytes(urlParameters);
                        wr.flush();
                    }

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'POST' request to URL : " + url);
                    System.out.println("Post parameters : " + urlParameters);
                    System.out.println("Response Code : " + responseCode);

                    if (responseCode == 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream(), "UTF-8"));
                        String inputLine = "";
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        result = response.toString();
                    } else {
                        result = Integer.toString(responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                }
                postExecuteOnUi(result);
            }


            private void postExecuteOnUi(final String result) {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            progressBar.setVisibility(View.INVISIBLE);
                            unfreezeScreen();
                            pd.dismiss();
                        } catch (Exception e) {
                            return;
                        }

                        if (result != null) {

                            if (result.contains("No Connection")) {
                                final Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG);

                                snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getDistricts();
                                        snackbar.dismiss();
                                    }
                                });

                                // Changing message text color
                                snackbar.setActionTextColor(Color.RED);

                                // Changing action button text color
                                View sbView = snackbar.getView();
                                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                                textView.setTextColor(Color.YELLOW);

                                snackbar.show();
                                logFirebaseErrorEvent(districtName, tehsilName, "", "", "");
                                return;
                            }

                            try {
                                ObjectMapper mapper = new ObjectMapper();
                                ArrayList<Village> villages = mapper.readValue(result, new TypeReference<ArrayList<Village>>() {
                                });

                                Intent intent = new Intent(activity, VillageActivity.class);
                                intent.putExtra(CommonData.VILLAGE_LIST, villages);
                                intent.putExtra(CommonData.CHOSEN_DISTRICT, districtName);
                                intent.putExtra(CommonData.CHOSEN_DISTRICT_CODE, districtCode);
                                intent.putExtra(CommonData.CHOSEN_DISTRICT_LOCALE, districtNameLocale);
                                intent.putExtra(CommonData.CHOSEN_TEHSIL, tehsilName);
                                intent.putExtra(CommonData.CHOSEN_TEHSIL_CODE, tehsilCode);
                                intent.putExtra(CommonData.CHOSEN_TEHSIL_LOCALE, tehsilNameLocale);

                                activity.startActivity(intent);

                            } catch (Exception e) {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                logFirebaseErrorEvent(districtName, tehsilName, "", "", "");
                            }
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            logFirebaseErrorEvent(districtName, tehsilName, "", "", "");
                        }
                    }
                });
            }
        };
        executors.submit(runnable);
    }


    public void searchKhata(String districtName, String districtCode, String districtNameLocale, String tehsilName, String tehsilCode, String tehsilNameLocale, String villageName, String villageCode, String villageNameLocale, String parganaName, String parganaCode, String action, NameValue nameValue, String fasliName, String fasliCode) {
        if (CommonData.isNetworkAvailable(activity.getApplicationContext())) {
            searchKhataAync(districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale, villageName, villageCode, villageNameLocale, parganaName, parganaCode, action, nameValue, fasliName, fasliCode);
        } else {
            final Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, activity.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_INDEFINITE);

            snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    searchKhata(districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale, villageName, villageCode, villageNameLocale, parganaName, parganaCode, action, nameValue, fasliName, fasliCode);
                    snackbar.dismiss();
                }
            });

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);

            snackbar.show();
        }
    }

    public void searchKhataAync(String districtName, String districtCode, String districtNameLocale, String tehsilName, String tehsilCode, String tehsilNameLocale, String villageName, String villageCode, String villageNameLocale, String parganaName, String parganaCode, String action, NameValue nameValue, String fasliName, String fasliCode) {
        ExecutorService executors = Executors.newFixedThreadPool(1);

        Runnable runnable = new Runnable() {
            private void preExecuteOnUi() {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.show();
                        progressBar.setVisibility(View.VISIBLE);
                        freezeScreen();
                    }
                });
            }

            @Override
            public void run() {
                String result = null;
                HttpURLConnection con = null;
                try {
                    preExecuteOnUi();
                    String baseLink = CommonData.UttarPradesh.bhulekhCopy;

                    URL url = new URL(baseLink);
                    con = (HttpURLConnection)
                            url.openConnection();

                    con.setRequestMethod("POST");
                    con.setRequestProperty("User-Agent", CommonData.USER_AGENT);
                    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                    con.setRequestProperty("Content-Type", CommonData.CONTENT_TYPE);
                    con.setRequestProperty("Host", CommonData.UttarPradesh.host);
                    con.setRequestProperty("Origin", CommonData.UttarPradesh.origin);
                    con.setRequestProperty("Referer", CommonData.UttarPradesh.referer);
                    con.setRequestProperty("Cookie", webCookieManager.getCookie(CommonData.UttarPradesh.baseURL));

                    String urlParameters = "act=" + action + "&vcc=" + villageCode + "&fasli-code-value=" + fasliCode + "&fasli-name-value=" + fasliName + "&" + nameValue.getName() + "=" + nameValue.getValue();
                    byte[] bytes = urlParameters.getBytes(StandardCharsets.UTF_8);

                    // Send post request
                    con.setDoOutput(true);
                    try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                        wr.write(bytes);
                        wr.flush();
                    }

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'POST' request to URL : " + url);
                    System.out.println("Post parameters : " + urlParameters);
                    System.out.println("Response Code : " + responseCode);

                    if (responseCode == 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream(), "UTF-8"));
                        String inputLine = "";
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        result = response.toString();
                    } else {
                        result = Integer.toString(responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                }
                postExecuteOnUi(result);
            }


            private void postExecuteOnUi(final String result) {
                Handler uiThread = new Handler(Looper.getMainLooper());
                uiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            progressBar.setVisibility(View.INVISIBLE);
                            unfreezeScreen();
                            pd.dismiss();
                        } catch (Exception e) {
                            return;
                        }

                        if (result != null) {

                            if (result.contains("No Connection")) {
                                final Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG);

                                snackbar.setAction(activity.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getDistricts();
                                        snackbar.dismiss();
                                    }
                                });

                                // Changing message text color
                                snackbar.setActionTextColor(Color.RED);

                                // Changing action button text color
                                View sbView = snackbar.getView();
                                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                                textView.setTextColor(Color.YELLOW);

                                snackbar.show();
                                logFirebaseErrorEvent(districtName, tehsilName, villageName, fasliName, action);
                                return;
                            }

                            try {
                                ObjectMapper mapper = new ObjectMapper();
                                ArrayList<SearchResult> searchResults = mapper.readValue(result, new TypeReference<ArrayList<SearchResult>>() {
                                });

                                if (searchResults != null && searchResults.size() > 0) {
                                    Intent intent = new Intent(activity, SearchResultActivity.class);
                                    intent.putExtra(CommonData.SEARCH_RESULT, searchResults);
                                    intent.putExtra(CommonData.CHOSEN_DISTRICT, districtName);
                                    intent.putExtra(CommonData.CHOSEN_DISTRICT_CODE, districtCode);
                                    intent.putExtra(CommonData.CHOSEN_DISTRICT_LOCALE, districtNameLocale);
                                    intent.putExtra(CommonData.CHOSEN_TEHSIL, tehsilName);
                                    intent.putExtra(CommonData.CHOSEN_TEHSIL_CODE, tehsilCode);
                                    intent.putExtra(CommonData.CHOSEN_TEHSIL_LOCALE, tehsilNameLocale);
                                    intent.putExtra(CommonData.CHOSEN_VILLAGE, villageName);
                                    intent.putExtra(CommonData.CHOSEN_VILLAGE_CODE, villageCode);
                                    intent.putExtra(CommonData.CHOSEN_VILLAGE_LOCALE, villageNameLocale);
                                    intent.putExtra(CommonData.PARGANA_NAME, parganaName);
                                    intent.putExtra(CommonData.PARGANA_CODE, parganaCode);
                                    intent.putExtra(CommonData.FASLI_NAME, fasliName);
                                    intent.putExtra(CommonData.FASLI_CODE, fasliCode);
                                    intent.putExtra(CommonData.ACTION, action);

                                    activity.startActivity(intent);
                                } else {
                                    Snackbar snackbar = Snackbar
                                            .make(coordinatorLayout, activity.getResources().getString(R.string.no_information_available), Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }
                            } catch (Exception e) {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                logFirebaseErrorEvent(districtName, tehsilName, villageName, fasliName, action);
                            }
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, activity.getResources().getString(R.string.sorry_error_occured), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            logFirebaseErrorEvent(districtName, tehsilName, villageName, fasliName, action);
                        }
                    }
                });
            }
        };
        executors.submit(runnable);
    }

    public void logFirebaseErrorEvent(String district, String taluka, String village, String
            fasli, String action) {
        ((DashboardActivity) activity).logFirebaseErrorEvent(district, taluka, village, fasli, action);
    }

    private void setCookies(URI baseUri) {
        cookieContent = "";

        try {
            if (rawCookieStore == null) {
                CookieHandler.setDefault(new java.net.CookieManager());
                rawCookieStore = ((java.net.CookieManager) CookieHandler.getDefault())
                        .getCookieStore();
            }
            if (rawCookieStore != null) {
                List<HttpCookie> cookies = rawCookieStore.get(baseUri);

                for (HttpCookie cookie : cookies) {
                    String setCookie = new StringBuilder(
                            cookie.toString()).append("; domain=")
                            .append(cookie.getDomain())
                            .append("; path=").append(cookie.getPath())
                            .toString();

                    if (cookie.toString().contains("JSESSIONID")) {
                        cookieContent = setCookie;
                    }

                    webCookieManager.setCookie(CommonData.UttarPradesh.baseURL, setCookie);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        CookieSyncManager.getInstance().sync();
                    }
                }
            }

        } catch (Exception e) {

        }
    }
}