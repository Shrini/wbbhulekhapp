package com.effigrity.wbbhulekh.model;

import java.io.Serializable;

public class Tehsil implements Serializable {
    private String tehsil_name;
    private String tehsil_code_census;
    private String tehsil_name_english;

    public String getTehsil_name() {
        return tehsil_name;
    }

    public void setTehsil_name(String tehsil_name) {
        this.tehsil_name = tehsil_name;
    }

    public String getTehsil_code_census() {
        return tehsil_code_census;
    }

    public void setTehsil_code_census(String tehsil_code_census) {
        this.tehsil_code_census = tehsil_code_census;
    }

    public String getTehsil_name_english() {
        return tehsil_name_english;
    }

    public void setTehsil_name_english(String tehsil_name_english) {
        this.tehsil_name_english = tehsil_name_english;
    }
}
