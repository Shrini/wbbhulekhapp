package com.effigrity.wbbhulekh;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.effigrity.wbbhulekh.model.UPSavedContent;
import com.effigrity.wbbhulekh.util.Appirater;
import com.effigrity.wbbhulekh.util.CommonData;
import com.effigrity.wbbhulekh.util.DbOperation;
import com.google.android.material.snackbar.Snackbar;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class SavedResultDisplay extends DashboardActivity {

    WebView webView;
    UPSavedContent savedContent;
    DbOperation op;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_display);

        Appirater.significantEvent(getApplicationContext(), SavedResultDisplay.this);

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.app_name));

        savedContent = (UPSavedContent) getIntent().getSerializableExtra(CommonData.SAVED_CONTENT);

        webView = (WebView) findViewById(R.id.webView1);
        //webView.setWebChromeClient(new WebChromeClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        //webSettings.setDisplayZoomControls(true);
        webSettings.setSupportZoom(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.loadDataWithBaseURL(CommonData.UttarPradesh.baseURL, savedContent.getContent(), "text/html; application/xhtml+xml; image/jxr; */*; charset=UTF-8",
                "UTF-8", null);

        op = new DbOperation(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.delete, menu);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            menu.findItem(R.id.action_save_as_pdf).setVisible(false);
            toast_long(this.getResources().getString(R.string.pdf_not_available));
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        Snackbar snackbar;
        switch (id) {

            case R.id.action_save_as_pdf:
                saveAsPDF("test", webView);
                break;

            case R.id.action_delete:
                op.Open();
                if (op.deleteSaved(savedContent.getDistrictName(), savedContent.getTehsilName(), savedContent.getVillageName(), savedContent.getKhataNumber(), savedContent.getDisplayName(), savedContent.getFasilName(), savedContent.getType())) {
                    toast_short(this.getResources().getString(R.string.delete));
                    op.close();
                    super.onBackPressed();
                } else {
                    toast_short(this.getResources().getString(R.string.could_not_delete));
                }
                op.close();
                break;

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(19)
    @SuppressWarnings("deprecation")
    private void saveAsPDF(String str, WebView webView) {
        try {
            PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

            String jobName = getApplicationContext().getString(R.string.app_name) + " Document";
            PrintDocumentAdapter printAdapter;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                printAdapter = webView.createPrintDocumentAdapter(jobName);
            } else {
                printAdapter = webView.createPrintDocumentAdapter();
            }

            PrintJob print = printManager.print(str, printAdapter, new PrintAttributes.Builder().build());
            printManager.getPrintJobs();
            new Thread(new PDFThread(this, print)).start();
        } catch (Exception e) {
            toast_long(this.getResources().getString(R.string.pdf_not_supported));
        }
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (NoSuchMethodException e) {
                } catch (Exception e) {
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    @TargetApi(19)
    class PDFThread implements Runnable {
        final PrintJob f2910a;
        final SavedResultDisplay f2911b;

        PDFThread(SavedResultDisplay c0965d, PrintJob printJob) {
            this.f2911b = c0965d;
            this.f2910a = printJob;
        }

        public void run() {
            do {
            } while (!this.f2910a.isCompleted());
        }
    }

    @Override
    public void onBackPressed() {
        CommonData.savedContent = null;
        CommonData.result = null;
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
