package com.effigrity.wbbhulekh.khatanicopy;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.effigrity.wbbhulekh.DashboardActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.util.Appirater;
import com.effigrity.wbbhulekh.util.CommonData;
import com.effigrity.wbbhulekh.util.DbOperation;
import com.google.android.material.snackbar.Snackbar;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.CookieHandler;

public class LastScreen extends DashboardActivity {

    WebView webView;

    String result = null;

    ProgressBar progressBar;
    Snackbar loadingBar;

    android.webkit.CookieManager webCookieManager;
    java.net.CookieStore rawCookieStore;

    boolean captchaPageDisplayed = false;
    public CoordinatorLayout coordinatorLayout;


    String districtName, districtCode, districtNameLocale;
    String tehsilName, tehsilCode, tehsilNameLocale;
    String villageName, villageCode, villageNameLocale;
    String parganaName, parganaCode;
    String fasliCode, fasliName, khataNumber;
    private int noOfTimes = 0;

    DbOperation op;
    private String displayName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.last_screen);

        DashboardActivity.showAd = false;

        Appirater.significantEvent(getApplicationContext(), this);

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        progressBar = findViewById(R.id.progressbar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadingBar = Snackbar
                .make(coordinatorLayout, getResources().getString(R.string.loading_data), Snackbar.LENGTH_INDEFINITE);

        View snackBarView = loadingBar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));


        districtName = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT);
        districtNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_LOCALE);
        districtCode = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_CODE);

        tehsilName = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL);
        tehsilCode = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_CODE);
        tehsilNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_LOCALE);

        villageName = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE);
        villageCode = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE_CODE);
        villageNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE_LOCALE);

        parganaName = getIntent().getStringExtra(CommonData.PARGANA_NAME);
        parganaCode = getIntent().getStringExtra(CommonData.PARGANA_CODE);

        fasliCode = getIntent().getStringExtra(CommonData.FASLI_CODE);
        fasliName = getIntent().getStringExtra(CommonData.FASLI_NAME);

        khataNumber = getIntent().getStringExtra(CommonData.KHATA_NUMBER);
        displayName = getIntent().getStringExtra(CommonData.DISPLAY_NAME);

        webView = (WebView) findViewById(R.id.webview1);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);

        try {
            webCookieManager = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.createInstance(LastScreen.this);
            }
            webCookieManager.setAcceptCookie(true);
            CookieHandler.setDefault(new java.net.CookieManager());
            rawCookieStore = ((java.net.CookieManager) CookieHandler.getDefault())
                    .getCookieStore();
        } catch (Exception e) {
            e.printStackTrace();
        }

        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                /*
                 * This call inject JavaScript into the page which just finished
                 * loading.
                 */
                try {
                    loadingBar.dismiss();
                    progressBar.setVisibility(View.INVISIBLE);
                    unfreezeScreen();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                //webView.loadUrl("javascript:document.txtCaptcha.focus()");
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                try {
                    loadingBar.show();
                    progressBar.setVisibility(View.VISIBLE);
                    freezeScreen();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(LastScreen.this);
                builder.setMessage(R.string.notification_error_ssl_cert_invalid);
                builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        onBackPressed();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        String urlParameters = "khata_number=" + khataNumber + "&fasli_code=" + fasliCode + "&fasli_name=" + fasliName + "&district_code=" + districtCode + "&district_name=" + districtName +
                "&tehsil_name=" + tehsilName + "&tehsil_code=" + tehsilCode + "&village_name=" + villageName +
                "&village_code=" + villageCode + "&pargana_name= (" + parganaName + ")&pargana_code=" + parganaCode;
        try {
            webView.postUrl(CommonData.UttarPradesh.captchaURL, urlParameters.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        webView.setInitialScale(100);
        op = new DbOperation(this);
    }

    class MyJavaScriptInterface {
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(String html) {
            loadingBar.dismiss();
            progressBar.setVisibility(View.INVISIBLE);
            unfreezeScreen();

            if (html.contains("captcha")) {
                toast_short(getApplicationContext().getString(R.string.enter_captcha));
                captchaPageDisplayed = true;
            } else if (html.contains(districtName)) {
                DashboardActivity.contentShown = true;
                result = html;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!isScreenFreezed()) {
            super.onBackPressed();
            if (mInterstitialAd.isLoaded() && DashboardActivity.contentShown) {
                mInterstitialAd.show();
            }
        } else {
            if (noOfTimes >= 3) {
                finish();
            }
            toast_short(getResources().getString(R.string.working_on_request));
            noOfTimes++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            menu.findItem(R.id.action_save_as_pdf).setVisible(false);
            toast_long(this.getResources().getString(R.string.pdf_not_available));
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        Snackbar snackbar;
        switch (id) {
            case R.id.action_save:
                if (DashboardActivity.contentShown) {
                    op.Open();
                    op.insertIntoSavedDb(districtName, tehsilName, villageName, khataNumber, displayName, fasliName, result, "ROR");
                    op.close();
                    toast_short(this.getResources().getString(R.string.saved));
                } else {
                    toast_short(this.getResources().getString(R.string.content_connot_be_saved));
                }
                break;

            case R.id.action_save_as_pdf:
                saveAsPDF("test", webView);
                break;

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(19)
    @SuppressWarnings("deprecation")
    private void saveAsPDF(String str, WebView webView) {
        try {
            PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

            String jobName = getApplicationContext().getString(R.string.app_name) + " Document";
            PrintDocumentAdapter printAdapter;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                printAdapter = webView.createPrintDocumentAdapter(jobName);
            } else {
                printAdapter = webView.createPrintDocumentAdapter();
            }

            PrintJob print = printManager.print(str, printAdapter, new PrintAttributes.Builder().build());
            printManager.getPrintJobs();
            new Thread(new PDFThread(this, print)).start();
        } catch (Exception e) {
            toast_long(this.getResources().getString(R.string.pdf_not_supported));
        }
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (NoSuchMethodException e) {
                } catch (Exception e) {
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    @TargetApi(19)
    class PDFThread implements Runnable {
        final PrintJob f2910a;
        final LastScreen f2911b;

        PDFThread(LastScreen c0965d, PrintJob printJob) {
            this.f2911b = c0965d;
            this.f2910a = printJob;
        }

        public void run() {
            do {
            } while (!this.f2910a.isCompleted());
        }
    }
}
