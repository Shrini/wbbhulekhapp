package com.effigrity.wbbhulekh.khatanicopy;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.asynctask.PropertyAsyncTask;
import com.effigrity.wbbhulekh.model.Tehsil;

import java.util.ArrayList;

public class TehsilAdapter extends BaseAdapter implements
        View.OnClickListener {

    private Activity activity;
    private ArrayList<Tehsil> tehsils;
    private static LayoutInflater inflater = null;
    String districtName, districtCode, districtNameLocale;
    CoordinatorLayout coordinatorLayout;
    ProgressBar progressBar;

    public TehsilAdapter(Activity a, ArrayList<Tehsil> tehsils, String districtName, String discrictCode, String districtNameLocale, CoordinatorLayout coordinatorLayout, ProgressBar progressBar) {
        activity = a;
        this.tehsils = tehsils;
        this.districtName = districtName;
        this.districtCode = discrictCode;
        this.districtNameLocale = districtNameLocale;
        this.coordinatorLayout = coordinatorLayout;
        this.progressBar = progressBar;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return tehsils.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.list_item, null);

        TextView name = vi.findViewById(R.id.name);
        name.setText((position + 1) + ") " + tehsils.get(position).getTehsil_name());

        vi.setTag(position);
        vi.setOnClickListener(this);
        return vi;
    }

    public void onClick(View v) {
        final int pos = (Integer) v.getTag();
        String tehsilName = tehsils.get(pos).getTehsil_name();
        String tehsilCode = tehsils.get(pos).getTehsil_code_census();
        String tehsilNameLocale = tehsils.get(pos).getTehsil_name();
        PropertyAsyncTask propertyAsyncTask = new PropertyAsyncTask(activity, coordinatorLayout, progressBar);
        propertyAsyncTask.getVillages(districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale);
    }
}