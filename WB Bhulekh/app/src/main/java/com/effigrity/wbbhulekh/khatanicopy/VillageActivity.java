package com.effigrity.wbbhulekh.khatanicopy;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.DashboardActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.model.Village;
import com.effigrity.wbbhulekh.util.CommonData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class VillageActivity extends DashboardActivity {

    ListView listView;
    ProgressBar progressBar;

    AdView adView;
    ArrayList<Village> villages;
    VillageAdapter villageAdapter;
    CoordinatorLayout coordinatorLayout;
    String districtName, districtCode, districtNameLocale;
    String tehsilName, tehsilCode, tehsilNameLocale;

    TextView district, tehsil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.village);

        listView = findViewById(R.id.list);
        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        district = findViewById(R.id.districtName);
        tehsil = findViewById(R.id.tehsilName);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.khaitani_copy_title));

        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        villages = (ArrayList<Village>) getIntent().getSerializableExtra(CommonData.VILLAGE_LIST);
        districtName = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT);
        districtNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_LOCALE);
        districtCode = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_CODE);

        tehsilName = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL);
        tehsilCode = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_CODE);
        tehsilNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_LOCALE);

        district.setText(districtNameLocale);
        tehsil.setText(tehsilNameLocale);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView.setAdapter(null);
        villageAdapter = new VillageAdapter(this, villages, districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale, coordinatorLayout, progressBar);
        listView.setAdapter(villageAdapter);
    }
}
