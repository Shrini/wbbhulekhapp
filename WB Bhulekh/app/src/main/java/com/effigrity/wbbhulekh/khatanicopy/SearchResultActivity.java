package com.effigrity.wbbhulekh.khatanicopy;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.DashboardActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.model.SearchResult;
import com.effigrity.wbbhulekh.util.Appirater;
import com.effigrity.wbbhulekh.util.CommonData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class SearchResultActivity extends DashboardActivity {

    ListView listView;
    ProgressBar progressBar;

    AdView adView;
    ArrayList<SearchResult> searchResults;
    SearchResultAdapter searchResultAdapter;
    CoordinatorLayout coordinatorLayout;
    String districtName, districtCode, districtNameLocale;
    String tehsilName, tehsilCode, tehsilNameLocale;
    String villageName, villageCode, villageNameLocale;
    String parganaName, parganaCode;
    String fasliName, fasliCode, action;

    TextView district, tehsil, village, fasli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result);

        listView = findViewById(R.id.list);
        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        district = findViewById(R.id.districtName);
        tehsil = findViewById(R.id.tehsilName);
        village = findViewById(R.id.villageName);
        fasli = findViewById(R.id.fasliName);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.khaitani_copy_title));

        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        searchResults = (ArrayList<SearchResult>) getIntent().getSerializableExtra(CommonData.SEARCH_RESULT);
        districtName = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT);
        districtNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_LOCALE);
        districtCode = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_CODE);

        tehsilName = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL);
        tehsilCode = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_CODE);
        tehsilNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_LOCALE);

        villageName = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE);
        villageCode = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE_CODE);
        villageNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE_LOCALE);

        parganaName = getIntent().getStringExtra(CommonData.PARGANA_NAME);
        parganaCode = getIntent().getStringExtra(CommonData.PARGANA_CODE);

        fasliName = getIntent().getStringExtra(CommonData.FASLI_NAME);
        fasliCode = getIntent().getStringExtra(CommonData.FASLI_CODE);
        action = getIntent().getStringExtra(CommonData.ACTION);

        district.setText(districtNameLocale);
        tehsil.setText(tehsilNameLocale);
        village.setText(villageNameLocale);
        fasli.setText(fasliName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView.setAdapter(null);
        searchResultAdapter = new SearchResultAdapter(this, searchResults, districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale, villageName, villageCode, villageNameLocale, parganaName, parganaCode, fasliCode, fasliName, action, coordinatorLayout, progressBar);
        listView.setAdapter(searchResultAdapter);
        if (DashboardActivity.contentShown = true) {
            Appirater.showInAppReview(getApplicationContext(), this);
        }
        DashboardActivity.contentShown = false;
    }
}
