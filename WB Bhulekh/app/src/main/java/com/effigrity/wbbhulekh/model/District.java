package com.effigrity.wbbhulekh.model;

import java.io.Serializable;

public class District implements Serializable {
    private String district_name;
    private String district_code_census;
    private String district_name_english;

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getDistrict_code_census() {
        return district_code_census;
    }

    public void setDistrict_code_census(String district_code_census) {
        this.district_code_census = district_code_census;
    }

    public String getDistrict_name_english() {
        return district_name_english;
    }

    public void setDistrict_name_english(String district_name_english) {
        this.district_name_english = district_name_english;
    }
}
