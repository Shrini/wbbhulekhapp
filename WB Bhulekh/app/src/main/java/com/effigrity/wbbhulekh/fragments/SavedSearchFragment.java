package com.effigrity.wbbhulekh.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.SavedKhataAdapter;
import com.effigrity.wbbhulekh.model.UPSavedContent;
import com.effigrity.wbbhulekh.util.DbOperation;

import java.util.ArrayList;


/**
 * Created by Shrinivas on 24-Nov-21.
 */
public class SavedSearchFragment extends Fragment {

    Activity activity;
    ListView lv;

    ArrayList<UPSavedContent> savedContents;
    SavedKhataAdapter savedKhataAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_saved, container, false);

        lv = rootView.findViewById(R.id.list);

        activity = getActivity();
        return rootView;
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            lv.setAdapter(null);

            DbOperation db = new DbOperation(activity);
            db.Open();
            savedContents = db.getContent();
            db.close();

            savedKhataAdapter = new SavedKhataAdapter(activity, savedContents);
            lv.setAdapter(savedKhataAdapter);
        } catch (Exception e) {
            Toast.makeText(getActivity(), R.string.sorry_error_occured, Toast.LENGTH_SHORT).show();
        }
    }

}
