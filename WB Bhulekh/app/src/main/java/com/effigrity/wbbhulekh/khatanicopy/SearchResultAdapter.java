package com.effigrity.wbbhulekh.khatanicopy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.model.SearchResult;
import com.effigrity.wbbhulekh.util.CommonData;

import java.util.ArrayList;

public class SearchResultAdapter extends BaseAdapter implements
        View.OnClickListener {

    private Activity activity;
    private ArrayList<SearchResult> searchResults;
    private static LayoutInflater inflater = null;
    CoordinatorLayout coordinatorLayout;
    ProgressBar progressBar;

    String districtName, districtCode, districtNameLocale;
    String tehsilName, tehsilCode, tehsilNameLocale;
    String villageName, villageCode, villageNameLocale, parganaName, parganaCode;
    String fasliCode, fasliName, action;

    public SearchResultAdapter(Activity a, ArrayList<SearchResult> searchResults, String districtName, String districtCode, String districtNameLocale, String tehsilName, String tehsilCode, String tehsilNameLocale, String villageName, String villageCode, String villageNameLocale, String praganaName, String praganaCode, String fasliCode, String fasliName, String action, CoordinatorLayout coordinatorLayout, ProgressBar progressBar) {
        activity = a;
        this.searchResults = searchResults;
        this.districtName = districtName;
        this.districtCode = districtCode;
        this.districtNameLocale = districtNameLocale;

        this.tehsilName = tehsilName;
        this.tehsilCode = tehsilCode;
        this.tehsilNameLocale = tehsilNameLocale;

        this.villageName = villageName;
        this.villageCode = villageCode;
        this.villageNameLocale = villageNameLocale;

        this.parganaName = praganaName;
        this.parganaCode = praganaCode;

        this.fasliCode = fasliCode;
        this.fasliName = fasliName;
        this.action = action;

        this.coordinatorLayout = coordinatorLayout;
        this.progressBar = progressBar;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return searchResults.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.list_item, null);

        TextView name = vi.findViewById(R.id.name);
        name.setText((position + 1) + ") " + searchResults.get(position).toString(action));

        vi.setTag(position);
        vi.setOnClickListener(this);
        return vi;
    }

    public void onClick(View v) {
        final int pos = (Integer) v.getTag();

        Intent intent = new Intent(activity, LastScreen.class);

        intent.putExtra(CommonData.CHOSEN_DISTRICT, districtName);
        intent.putExtra(CommonData.CHOSEN_DISTRICT_CODE, districtCode);
        intent.putExtra(CommonData.CHOSEN_DISTRICT_LOCALE, districtNameLocale);

        intent.putExtra(CommonData.CHOSEN_TEHSIL, tehsilName);
        intent.putExtra(CommonData.CHOSEN_TEHSIL_CODE, tehsilCode);
        intent.putExtra(CommonData.CHOSEN_TEHSIL_LOCALE, tehsilNameLocale);

        intent.putExtra(CommonData.CHOSEN_VILLAGE, villageName);
        intent.putExtra(CommonData.CHOSEN_VILLAGE_CODE, villageCode);
        intent.putExtra(CommonData.CHOSEN_VILLAGE_LOCALE, villageName);

        intent.putExtra(CommonData.PARGANA_NAME, parganaName);
        intent.putExtra(CommonData.PARGANA_CODE, parganaCode);

        intent.putExtra(CommonData.FASLI_CODE, fasliCode);
        intent.putExtra(CommonData.FASLI_NAME, fasliName);

        intent.putExtra(CommonData.KHATA_NUMBER, searchResults.get(pos).getKhata_number());
        intent.putExtra(CommonData.DISPLAY_NAME, searchResults.get(pos).toString(action));

        activity.startActivity(intent);
    }
}