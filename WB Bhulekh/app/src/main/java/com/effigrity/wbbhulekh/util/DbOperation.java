package com.effigrity.wbbhulekh.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.effigrity.wbbhulekh.model.UPSavedContent;

import java.util.ArrayList;


/**
 * Created by I069067 on 14-Sep-16.
 */
public class DbOperation {

    public static String DISTRICT_ID = "DISTRICT_ID";
    public static String TEHSIL_ID = "TEHSIL_ID";
    public static String VILLAGE_ID = "VILLAGE_ID";
    public static String FASLI_YEAR = "FASLI_YEAR";
    public static String TYPE = "TYPE";
    public static String KHATA_NUMBER = "KHATA_NUMBER";
    public static String DISPLAY_NAME = "DISPLAY_NAME";
    public static String CONTENT = "CONTENT";

    private DBHelper ourHelper;
    private final Context ourcontext;
    private SQLiteDatabase ourDatabase;

    private static final String DATABASE_NAME = "_UP_BHULEKH_";
    private static final String DATABASE_TABLE1 = "UP_SAVED_SEARCHES";

    private static final int DATABASE_VERSION = 1;

    private static class DBHelper extends SQLiteOpenHelper {
        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) throws SQLException {
            String Query = "CREATE TABLE " + DATABASE_TABLE1 + "("
                    + DISTRICT_ID + " TEXT , " + TEHSIL_ID
                    + " TEXT , " + VILLAGE_ID + " TEXT , " + KHATA_NUMBER + " TEXT , "
                    + DISPLAY_NAME + " TEXT , " + FASLI_YEAR + " TEXT , " + TYPE + " TEXT , " + CONTENT + " TEXT );";

            db.execSQL(Query);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    public DbOperation(Context c) {
        ourcontext = c;
    }

    public DbOperation Open() throws SQLException {
        ourHelper = new DBHelper(ourcontext);
        ourDatabase = ourHelper.getWritableDatabase();
        ourHelper.onUpgrade(ourDatabase, 1, 2);
        return this;

    }

    public void close() {
        ourHelper.close();
        ourDatabase.close();
    }

    public void insertIntoSavedDb(String districtId, String tehdilId, String villageId, String khataNumber, String displayName, String fasliName, String content, String type) {

        ContentValues cv = new ContentValues();
        cv.put(DISTRICT_ID, districtId);
        cv.put(TEHSIL_ID, tehdilId);
        cv.put(VILLAGE_ID, villageId);
        cv.put(KHATA_NUMBER, khataNumber);
        cv.put(DISPLAY_NAME, displayName);
        cv.put(FASLI_YEAR, fasliName);
        cv.put(CONTENT, content);
        cv.put(TYPE, type);

        if (!isDataAlreadyPresent(districtId, tehdilId, villageId, khataNumber, displayName, fasliName, type)) {
            ourDatabase.insert(DATABASE_TABLE1, null, cv);
        } else {
            deleteSaved(districtId, tehdilId, villageId, khataNumber, displayName, fasliName, type);
            ourDatabase.insert(DATABASE_TABLE1, null, cv);
        }

    }

    private boolean isDataAlreadyPresent(String districtId, String tehsilId, String villageId, String khataNumber, String displayName, String fasliName, String type) {
        String query = DISTRICT_ID + " = '" + districtId + "' AND " + TEHSIL_ID + " = '" + tehsilId + "' AND " + VILLAGE_ID + " = '" + villageId + "' AND " + KHATA_NUMBER + " = '" + khataNumber + "' AND " + DISPLAY_NAME + " = '" + displayName + "' AND " + FASLI_YEAR + " = '" + fasliName + "' AND " + TYPE + " = '" + type + "'";

        String[] columns = new String[]{DISTRICT_ID};
        Cursor c = ourDatabase.query(DATABASE_TABLE1, columns, query, null,
                null, null, null);

        if (c != null) {
            c.moveToFirst();
            if (c.getCount() > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean deleteSaved(String districtId, String tehsilId, String villageId, String khataNumber, String displayName, String fasliName, String type) {
        String query = DISTRICT_ID + " = '" + districtId + "' AND " + TEHSIL_ID + " = '" + tehsilId + "' AND " + VILLAGE_ID + " = '" + villageId + "' AND " + KHATA_NUMBER + " = '" + khataNumber + "' AND " + DISPLAY_NAME + " = '" + displayName + "' AND " + FASLI_YEAR + " = '" + fasliName + "' AND " + TYPE + " = '" + type + "'";

        if (ourDatabase.delete(DATABASE_TABLE1, query,
                null) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<UPSavedContent> getContent() {

        ArrayList<UPSavedContent> tobeReturned = new ArrayList<>();

        String[] columns = new String[]{DISTRICT_ID, TEHSIL_ID,
                VILLAGE_ID, KHATA_NUMBER, DISPLAY_NAME, FASLI_YEAR, CONTENT, TYPE};

        Cursor c = ourDatabase.query(DATABASE_TABLE1, columns, null, null,
                null, null, null, null);

        if (c != null) {
            c.moveToFirst();
            if (c.getCount() > 0) {
                for (; !c.isAfterLast(); c.moveToNext()) {
                    UPSavedContent temp = new UPSavedContent();
                    temp.setDistrictName(c.getString(c
                            .getColumnIndex(DISTRICT_ID)));
                    temp.setTehsilName(c.getString(c.getColumnIndex(TEHSIL_ID)));
                    temp.setVillageName(c.getString(c
                            .getColumnIndex(VILLAGE_ID)));
                    temp.setKhataNumber(c.getString(c.getColumnIndex(KHATA_NUMBER)));
                    temp.setDisplayName(c.getString(c.getColumnIndex(DISPLAY_NAME)));
                    temp.setFasilName(c.getString(c.getColumnIndex(FASLI_YEAR)));
                    temp.setContent(c.getString(c.getColumnIndex(CONTENT)));
                    temp.setType(c.getString(c.getColumnIndex(TYPE)));
                    tobeReturned.add(temp);
                }
            }
        }

        return tobeReturned;
    }
}
