package com.effigrity.wbbhulekh.fragments;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.effigrity.wbbhulekh.HomeActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.asynctask.CommonAsyncTask;
import com.effigrity.wbbhulekh.asynctask.PropertyAsyncTask;
import com.effigrity.wbbhulekh.util.CommonData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


/**
 * Created by I069067 on 31-Aug-16.
 */
public class MainFragment extends Fragment {

    Activity activity;

    Button khitaniCopy, khaitaniShare;

    AdView adView;
    ProgressBar progressBar;

    CommonAsyncTask commonAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        khitaniCopy = rootView.findViewById(R.id.khaitani_copy);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        activity = getActivity();
        progressBar.setVisibility(View.INVISIBLE);

        khitaniCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PropertyAsyncTask propertyAsyncTask = new PropertyAsyncTask(activity, MainFragment.this, ((HomeActivity) activity).coordinatorLayout, progressBar);
                propertyAsyncTask.getDistricts();
            }
        });


        commonAsyncTask = new CommonAsyncTask(activity, this, ((HomeActivity) activity).coordinatorLayout);

        adView = rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        commonAsyncTask.checkAppVersion();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public synchronized void displaNotification() {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager mNotificationManager = (NotificationManager) activity
                .getSystemService(Context.NOTIFICATION_SERVICE);
        CharSequence text = activity.getResources().getString(R.string.app_name);
        CharSequence contentTitle = activity.getResources().getString(R.string.version_update);
        CharSequence contentText = activity.getResources().getString(R.string.new_version);

        long when = System.currentTimeMillis();

        Intent ii = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + CommonData.appName));

        int NOTIFICATION_ID = (int) (Math.random() * 1000);

        PendingIntent contentIntent = PendingIntent.getActivity(activity,
                NOTIFICATION_ID, ii, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                activity)
                .setSmallIcon(R.drawable.ic_launcher_transperant)
                .setAutoCancel(true)
                .setSound(alarmSound)
                .setContentTitle(contentTitle)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(text)).setContentText(contentText);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
