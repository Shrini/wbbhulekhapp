package com.effigrity.wbbhulekh.khatanicopy;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.asynctask.PropertyAsyncTask;
import com.effigrity.wbbhulekh.model.District;

import java.util.ArrayList;

public class DistrictAdapter extends BaseAdapter implements
        View.OnClickListener {

    private Activity activity;
    private ArrayList<District> districts;
    private static LayoutInflater inflater = null;
    CoordinatorLayout coordinatorLayout;
    ProgressBar progressBar;

    public DistrictAdapter(Activity a, ArrayList<District> districts, CoordinatorLayout coordinatorLayout, ProgressBar progressBar) {
        activity = a;
        this.districts = districts;
        this.coordinatorLayout = coordinatorLayout;
        this.progressBar = progressBar;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return districts.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.list_item, null);

        TextView name = vi.findViewById(R.id.name);
        name.setText((position + 1) + ") " + districts.get(position).getDistrict_name());

        vi.setTag(position);
        vi.setOnClickListener(this);
        return vi;
    }

    public void onClick(View v) {
        final int pos = (Integer) v.getTag();
        String districtName = districts.get(pos).getDistrict_name();
        String districtCode = districts.get(pos).getDistrict_code_census();
        String districtNameLocale = districts.get(pos).getDistrict_name();
        PropertyAsyncTask propertyAsyncTask = new PropertyAsyncTask(activity, coordinatorLayout, progressBar);
        propertyAsyncTask.getTehsils(districtName, districtCode, districtNameLocale);
    }
}