package com.effigrity.wbbhulekh.model;

import java.io.Serializable;

public class Village implements Serializable {
    private String vname;
    private String village_code_census;
    private String vname_eng;
    private String pname;
    private String flg_chakbandi;
    private String flg_survey;
    private String pargana_code_new;

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getVillage_code_census() {
        return village_code_census;
    }

    public void setVillage_code_census(String village_code_census) {
        this.village_code_census = village_code_census;
    }

    public String getVname_eng() {
        return vname_eng;
    }

    public void setVname_eng(String vname_eng) {
        this.vname_eng = vname_eng;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getFlg_chakbandi() {
        return flg_chakbandi;
    }

    public void setFlg_chakbandi(String flg_chakbandi) {
        this.flg_chakbandi = flg_chakbandi;
    }

    public String getFlg_survey() {
        return flg_survey;
    }

    public void setFlg_survey(String flg_survey) {
        this.flg_survey = flg_survey;
    }

    public String getPargana_code_new() {
        return pargana_code_new;
    }

    public void setPargana_code_new(String pargana_code_new) {
        this.pargana_code_new = pargana_code_new;
    }
}
