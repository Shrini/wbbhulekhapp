package com.effigrity.wbbhulekh;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;

public class About extends AppCompatActivity {

    public CoordinatorLayout coordinatorLayout;

    TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        String versionName = BuildConfig.VERSION_NAME;

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        version = (TextView) findViewById(R.id.version);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(" About Us");

        version.setText(getResources().getString(R.string.version) + versionName);
    }

    public void onClickContact(View v) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL,
                new String[]{"uttarpradesh.bhulekh@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "App: Enquiry");
        i.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(i, "Send mail"));
        } catch (android.content.ActivityNotFoundException ex) {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "There are no email clients installed.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}
