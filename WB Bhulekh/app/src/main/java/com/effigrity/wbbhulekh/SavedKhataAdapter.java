package com.effigrity.wbbhulekh;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.effigrity.wbbhulekh.model.UPSavedContent;
import com.effigrity.wbbhulekh.util.CommonData;

import java.util.ArrayList;

public class SavedKhataAdapter extends BaseAdapter implements
        View.OnClickListener {

    private Activity activity;
    private ArrayList<UPSavedContent> savedContents;
    private static LayoutInflater inflater = null;

    public SavedKhataAdapter(Activity a, ArrayList<UPSavedContent> savedContents) {
        activity = a;
        this.savedContents = savedContents;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return savedContents.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.saved_list_item, null);

        TextView district = vi.findViewById(R.id.district);
        TextView tehsil = vi.findViewById(R.id.taluka);
        TextView village = vi.findViewById(R.id.village);
        TextView displayName = vi.findViewById(R.id.name);

        district.setText(savedContents.get(position).getDistrictName());
        tehsil.setText(savedContents.get(position).getTehsilName());
        village.setText(savedContents.get(position).getVillageName());
        displayName.setText(savedContents.get(position).getDisplayName());

        vi.setTag(position);
        vi.setOnClickListener(this);
        return vi;
    }

    public void onClick(View v) {
        final int pos = (Integer) v.getTag();

        Intent intent = new Intent(activity, SavedResultDisplay.class);
        intent.putExtra(CommonData.SAVED_CONTENT, savedContents.get(pos));
        activity.startActivity(intent);
    }
}