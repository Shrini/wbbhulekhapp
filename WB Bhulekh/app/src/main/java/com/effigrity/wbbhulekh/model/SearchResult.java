package com.effigrity.wbbhulekh.model;

import java.io.Serializable;

public class SearchResult implements Serializable {
    private String khata_number;
    private String khasra_number;
    private String unique_code;
    private String name;
    private String father;

    public String getKhata_number() {
        return khata_number;
    }

    public void setKhata_number(String khata_number) {
        this.khata_number = khata_number;
    }

    public String getKhasra_number() {
        return khasra_number;
    }

    public void setKhasra_number(String khasra_number) {
        this.khasra_number = khasra_number;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String toString(String act) {
        switch (act) {
            case "sbksn":
                return khasra_number + ": " + unique_code;

            case "sbacn":
            case "sbname":
                return khata_number + ": " + name + ": " + father;

            default:
                return khata_number;
        }
    }
}
