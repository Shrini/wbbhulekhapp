package com.effigrity.wbbhulekh.khatanicopy;

import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.effigrity.wbbhulekh.DashboardActivity;
import com.effigrity.wbbhulekh.R;
import com.effigrity.wbbhulekh.asynctask.PropertyAsyncTask;
import com.effigrity.wbbhulekh.model.NameValue;
import com.effigrity.wbbhulekh.util.CommonData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.util.Strings;
import com.google.android.material.snackbar.Snackbar;
import com.satsuware.usefulviews.LabelledSpinner;

public class SearchInputActivity extends DashboardActivity implements View.OnClickListener {

    ProgressBar progressBar;

    AdView adView;
    CoordinatorLayout coordinatorLayout;

    String districtName, districtCode, districtNameLocale;
    String tehsilName, tehsilCode, tehsilNameLocale;
    String villageName, villageCode, villageNameLocale;
    String parganaName, parganaCode;

    TextView district, tehsil, village;
    LabelledSpinner fasliVarsh;

    String falsiCode = "999";
    String act = "sbksn";
    String fasliName;

    RadioButton searchByGata, searchByKhata, searchByName, searchByDate;
    Button search;
    EditText searchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_input);

        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        district = findViewById(R.id.districtName);
        tehsil = findViewById(R.id.tehsilName);
        village = findViewById(R.id.villageName);
        searchByGata = findViewById(R.id.searchByGataNumber);
        searchByKhata = findViewById(R.id.searchByKhataNumber);
        searchByName = findViewById(R.id.searchByName);
        searchByDate = findViewById(R.id.searchByDate);
        search = findViewById(R.id.search);
        searchText = findViewById(R.id.searchtext);
        searchText.setInputType(InputType.TYPE_CLASS_NUMBER);

        search.setOnClickListener(this);

        searchByGata.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    act = "sbksn";
                    searchText.requestFocus();
                    searchText.setHint(R.string.search_by_gata_number_hint);
                    searchText.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
            }
        });
        searchByKhata.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    act = "sbacn";
                    searchText.requestFocus();
                    searchText.setHint(R.string.search_by_khata_number_hint);
                    searchText.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
            }
        });
        searchByName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    act = "sbname";
                    searchText.setHint(R.string.search_by_name_hint);
                    searchText.setInputType(InputType.TYPE_CLASS_TEXT);
                    searchText.requestFocus();
                }
            }
        });
        searchByDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    act = "sbdate";
                    searchText.requestFocus();
                    searchText.setHint(R.string.search_by_date_hint);
                    searchText.setInputType(InputType.TYPE_CLASS_DATETIME);
                }
            }
        });

        fasliVarsh = findViewById(R.id.fasli_varsh_spinner);
        fasliVarsh.setItemsArray(getResources().getStringArray(R.array.fasli_varsh_option));
        fasliVarsh.setLabelText(this.getResources().getString(R.string.fasli_varsh));
        fasliVarsh.setColor(R.color.colorPrimaryDark);

        fasliName = getResources().getStringArray(R.array.fasli_varsh_option)[0];

        fasliVarsh.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                String[] falsiCodes = getResources().getStringArray(R.array.fasli_varsh_code);
                falsiCode = falsiCodes[position];
                fasliName = getResources().getStringArray(R.array.fasli_varsh_option)[position];
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.khaitani_copy_title));

        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        districtName = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT);
        districtNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_LOCALE);
        districtCode = getIntent().getStringExtra(CommonData.CHOSEN_DISTRICT_CODE);

        tehsilName = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL);
        tehsilCode = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_CODE);
        tehsilNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_TEHSIL_LOCALE);

        villageName = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE);
        villageCode = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE_CODE);
        villageNameLocale = getIntent().getStringExtra(CommonData.CHOSEN_VILLAGE_LOCALE);

        parganaName = getIntent().getStringExtra(CommonData.PARGANA_NAME);
        parganaCode = getIntent().getStringExtra(CommonData.PARGANA_CODE);

        district.setText(districtNameLocale);
        tehsil.setText(tehsilNameLocale);
        village.setText(villageNameLocale);
    }

    @Override
    public void onClick(View v) {
        if (Strings.isEmptyOrWhitespace(searchText.getText().toString())) {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, getResources().getString(R.string.enter_search_field), Snackbar.LENGTH_LONG);
            snackbar.show();
            searchText.requestFocus();
            return;
        }

        NameValue nameValue = new NameValue();
        switch (v.getId()) {
            case R.id.search:
                switch (act) {
                    case "sbksn":
                        nameValue.setName("kcn");
                        nameValue.setValue(searchText.getText().toString());
                        break;
                    case "sbacn":
                        nameValue.setName("acn");
                        String text = searchText.getText().toString();
                        while (text.length() < 5) {
                            text = "0" + text;
                        }
                        nameValue.setValue(text);
                        break;
                    case "sbname":
                        nameValue.setName("name");
                        nameValue.setValue(searchText.getText().toString());
                        break;
                    case "sbdate":
                        nameValue.setName("mut_date");
                        nameValue.setValue(searchText.getText().toString());
                        break;
                }
        }

        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }

        PropertyAsyncTask propertyAsyncTask = new PropertyAsyncTask(this, coordinatorLayout, progressBar);
        propertyAsyncTask.searchKhata(districtName, districtCode, districtNameLocale, tehsilName, tehsilCode, tehsilNameLocale, villageName, villageCode, villageNameLocale, parganaName, parganaCode, act, nameValue, fasliName, falsiCode);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
